Sort a linked list of 0s, 1s and 2s
Given a linked list of 0s, 1s and 2s, sort it.

Following steps can be used to sort the given linked list.
1) Traverse the list and count the number of 0s, 1s and 2s. Let the counts be n1, n2 and n3 respectively.
2) Traverse the list again, fill the first n1 nodes with 0, then n2 nodes with 1 and finally n3 nodes with 2.

Time Complexity: O(n) where n is number of nodes in linked list.
Auxiliary Space: O(1)

Output:
Linked List Before Sorting
2  1  2  1  1  2  0  1  0
Linked List After Sorting
0  0  1  1  1  1  2  2  2


Экскурсии
Туры по Парижу, и по всей Франции
PARISTUR.RU — это сервис по бронированию экскурсии, трансферов и аренды автомобилей, яхт и т.д, с водителем по самым выгодным ценам.
http://paristur.ru/

Horoscopes, Zodiac, Readings
astrology, horoscopes, tarot, psychic, chinese, vedic, mayan , numerology, feng shui,  zodiac 101, sun sign compatibility, video, readings
Zodiac Page - read, get horoscopes, online tarot readings, psychic readings, Chinese astrology, Vedic Astrology, Mayan Astrology, Numerology, Feng Shui,  zodiac 101, sun sign compatibility and video horoscopes.
http://www.zodiacpage.com/